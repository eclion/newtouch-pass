package com.newtouch.paas.shell;

import com.newtouch.lion.exception.BaseException;

/**
 * Created by wanglijun on 16/5/17.
 */
public class ShellException extends BaseException {

    public ShellException(String code,String msg,Throwable cause){
        super(code,msg,cause);
    }

}
