package com.newtouch.paas.shell.userinfo;

import com.jcraft.jsch.UserInfo;

/**
 * Created by wanglijun on 16/5/17.
 */
public class ShellUserInfo  implements UserInfo {
    /***用户名和密码登录方式*/
    public static final String LOGIN_TYPE_USERPWD="UP";
    /**SSH key方式登录*/
    public static final String LOGIN_TYPE_SSHKEY="SSHKEY";
    /**登录的默认端口:22*/
    public static final int DEFAULT_PORT=22;
    /***
     * 登录的用户名
     */
    private String username;
    /**登录的密码*/
    private String password;
    /**登录的服务器*/
    private String host;
    /**登录的端口号*/
    private int port=DEFAULT_PORT;
    /***
     *默认的登录方式为用户名或密码
     */
    private String loginType=LOGIN_TYPE_USERPWD;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }


    public ShellUserInfo() {
        super();
    }

    /***
     *
     * @param password
     * @param loginType
     * @param port
     * @param username
     */
    public ShellUserInfo(String password, String loginType, int port, String username) {
        this.password = password;
        this.loginType = loginType;
        this.port = port;
        this.username = username;
    }

    /**
     *
     * @param password
     * @param username
     */
    public ShellUserInfo(String password, String username) {
        this.password = password;
        this.username = username;
    }


    @Override
    public String getPassphrase() {
        return null;
    }

    @Override
    public boolean promptPassword(String s) {
        return false;
    }

    @Override
    public boolean promptPassphrase(String s) {
        return false;
    }

    @Override
    public boolean promptYesNo(String s) {
        return true;
    }

    @Override
    public void showMessage(String s) {

    }


    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
