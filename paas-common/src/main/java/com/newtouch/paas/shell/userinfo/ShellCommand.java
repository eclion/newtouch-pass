package com.newtouch.paas.shell.userinfo;

/**
 * Created by wanglijun on 16/5/17.
 */
public class ShellCommand {
    /***
     * 执行的命令
     */
    private String command;
    /***
     * Shell 登录信息
     */
    private ShellUserInfo shellUserInfo;
    /**执行结果*/
    private String executionResult;
    /**返回状态*/
    private int returnCode;

    /**
     *
     */
    public ShellCommand() {
        super();
    }

    /**
     *
     * @param command
     * @param shellUserInfo
     */
    public ShellCommand(String command, ShellUserInfo shellUserInfo) {
        this.command = command;
        this.shellUserInfo = shellUserInfo;
    }

    /***
     *
     * @param command
     * @param password
     * @param username
     */
    public ShellCommand(String command,String password, String username){
        this.command=command;
        this.shellUserInfo=new ShellUserInfo(password,username);
    }


    public ShellCommand(String command,String password, String loginType, int port, String username){
        this.command=command;
        this.shellUserInfo=new ShellUserInfo(password,loginType,port,username);
    }


    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getExecutionResult() {
        return executionResult;
    }

    public void setExecutionResult(String executionResult) {
        this.executionResult = executionResult;
    }

    public ShellUserInfo getShellUserInfo() {
        return shellUserInfo;
    }

    public void setShellUserInfo(ShellUserInfo shellUserInfo) {
        this.shellUserInfo = shellUserInfo;
    }


    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }
}
