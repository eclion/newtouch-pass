package com.paas.dao;

import com.newtouch.lion.dao.BaseDao;
import com.paas.model.test.UserTest;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company:
 * </p>
 *
 * @author LiXiaoHao
 * @version 1.0
 */
@Repository("userTestDao")
public interface UserTestDao extends BaseDao<UserTest,Long>{
}
