package com.paas.dao.impl;

import com.newtouch.lion.dao.impl.BaseDaoImpl;
import com.paas.dao.UserTestDao;
import com.paas.model.test.UserTest;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company:
 * </p>
 *
 * @author LiXiaoHao
 * @version 1.0
 */
public class UserTestDaoImpl extends BaseDaoImpl<UserTest,Long> implements UserTestDao {
}
