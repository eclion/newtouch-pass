package com.paas.model.test;

import com.newtouch.lion.model.VersionEntity;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company:
 * </p>
 *
 * @author LiXiaoHao
 * @version 1.0
 */
public class UserTest extends VersionEntity<Long> {

    private static final long serialVersionUID=-2196344082980302505L;
    private Long id;
    private Long age;
    private String name;

    public UserTest() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public Long getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }
}
