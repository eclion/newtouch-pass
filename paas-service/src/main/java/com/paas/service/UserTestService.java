package com.paas.service;

import com.paas.model.test.UserTest;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company:
 * </p>
 *
 * @author LiXiaoHao
 * @version 1.0
 */
public interface UserTestService {
    /***
     * 根据ID查询
     * @param id
     * @return
     */
    public UserTest doFindById(Long id);

    /***
     *新建一个UserTest
     * @param userTest
     */
    public void doCreate(UserTest userTest);

    /***
     *删除一个UserTest
     * @param id
     * @return
     */
    public int doDeleteById(Long id);
}
