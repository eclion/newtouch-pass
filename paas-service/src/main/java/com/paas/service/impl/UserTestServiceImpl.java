package com.paas.service.impl;

import com.paas.dao.UserTestDao;
import com.paas.model.test.UserTest;
import com.paas.service.UserTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company:
 * </p>
 *
 * @author LiXiaoHao
 * @version 1.0
 */
@Service("userTestService")
public class UserTestServiceImpl implements UserTestService {
    @Autowired
    private UserTestDao userTestDao;
    @Override
    public UserTest doFindById(Long id) {
        return this.userTestDao.findById(id);
    }

    @Override
    public void doCreate(UserTest userTest) {
        this.userTestDao.save(userTest);
    }

    @Override
    public int doDeleteById(Long id) {
        String hql="delete from UserTest u where u.i=:id";
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("id",id);
        return this.userTestDao.updateHQL(hql,params);
    }
}
